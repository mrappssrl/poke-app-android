package com.mrapps.pokeapp.state

import com.mrapps.pokeapp.model.Pokemon as PokemonModel
import com.mrapps.pokeapp.model.PokemonDetail
import io.uniflow.core.flow.data.UIState

sealed class AppState : UIState() {
    class Pokemon(val pokemon: PokemonModel, val detail: PokemonDetail) : AppState()
    class PokemonNotLoaded(val pokemon: PokemonModel) : AppState()
}

