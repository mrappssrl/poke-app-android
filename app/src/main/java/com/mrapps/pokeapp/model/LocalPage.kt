package com.mrapps.pokeapp.model

data class LocalPage(
    val total: Int = 0,
    val items: List<Pokemon> = emptyList(),
    val nextPage: Int? = null
)