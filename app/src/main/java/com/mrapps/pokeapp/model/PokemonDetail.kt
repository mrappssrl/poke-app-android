package com.mrapps.pokeapp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity(tableName = "pokemon_detail")
@JsonClass(generateAdapter = true)
data class PokemonDetail(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "name")
    @field:Json(name = "name")
    val name: String,

    @field:Json(name = "height") val height: Int,
    @field:Json(name = "weight") val weight: Int,
    @field:Json(name = "base_experience") val experience: Int,

    @ColumnInfo(name = "stats")
    @field:Json(name = "stats")
    val stats: List<StatValue>,

    @ColumnInfo(name = "types")
    @field:Json(name = "types")
    val types: List<TypeValue>
) {
    fun getFormattedWeight(): String = String.format("%.1f KG", weight.toFloat() / 10)
    fun getFormattedHeight(): String = String.format("%.1f M", height.toFloat() / 10)
}


@JsonClass(generateAdapter = true)
data class Stat(
    @field:Json(name = "name")
    val name: String
)


@JsonClass(generateAdapter = true)
data class StatValue(
    @field:Json(name = "base_stat")
    val baseStat: Long,

    @field:Json(name = "stat")
    val stat: Stat
)

@JsonClass(generateAdapter = true)
data class Type(
    @field:Json(name = "name")
    val name: String
)

@JsonClass(generateAdapter = true)
data class TypeValue(
    @field:Json(name = "type")
    val type: Type,

    @field:Json(name = "slot")
    val slot: Int
)