package com.mrapps.pokeapp.view.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.mrapps.pokeapp.R
import com.mrapps.pokeapp.model.Pokemon
import kotlinx.android.synthetic.main.item_pokemon.view.*

class PokemonViewHolder(
    parent: ViewGroup,
    private val onItemSelected: (pokemon: Pokemon) -> Unit
) :
    RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon, parent, false)
    ) {
    fun bindPokemon(item: Pokemon) = with(itemView) {
        pokemonName.text = item.name

        Glide.with(itemView.context)
            .load(item.getImageUrl())
            .transition(DrawableTransitionOptions.withCrossFade())
            .dontTransform()
            .into(pokemonImage)

        cardContainer.setOnClickListener { onItemSelected(item) }
    }
}