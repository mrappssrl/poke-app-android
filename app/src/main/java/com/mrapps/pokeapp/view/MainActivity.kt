package com.mrapps.pokeapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mrapps.pokeapp.R
import com.mrapps.pokeapp.viewModel.AppViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val appViewModel: AppViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        appViewModel.getCurrentState()
    }
}