package com.mrapps.pokeapp.view.list

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import com.mrapps.pokeapp.R
import com.mrapps.pokeapp.event.AppEvent
import com.mrapps.pokeapp.viewModel.AppViewModel
import io.uniflow.androidx.flow.onEvents
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ListFragment : Fragment(R.layout.fragment_list) {

    private val appViewModel: AppViewModel by sharedViewModel()
    private val adapter = PokemonAdapter { pokemon ->
        appViewModel.openPokemonDetail(pokemon)
    }

    private fun errorToast(message: String){
        Toast.makeText(
            requireContext(),
            "\uD83D\uDE28 Wooops $message",
            Toast.LENGTH_LONG
        ).show()
    }

    private val loadStateListener = { loadState: CombinedLoadStates ->
        val errorState = loadState.source.append as? LoadState.Error
            ?: loadState.source.prepend as? LoadState.Error
            ?: loadState.append as? LoadState.Error
            ?: loadState.prepend as? LoadState.Error
        errorState?.error?.message?.let {
            errorToast(it)
        }
        Unit
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setOnMenuItemClickListener { menuItem ->
            if (menuItem.itemId == R.id.refresh) {
                appViewModel.search()
            }
            menuItem.itemId == R.id.refresh
        }

        recyclerView.adapter = adapter.withLoadStateHeaderAndFooter(
            header = PokemonLoadStateAdapter { adapter.retry() },
            footer = PokemonLoadStateAdapter { adapter.retry() }
        )
        retryButton.setOnClickListener { adapter.retry() }

        onEvents(appViewModel) { event ->
            when (val data = event.take()) {
                is AppEvent.SendPagingData -> lifecycleScope.launch {
                    adapter.submitData(data.pagingData)
                }

                is AppEvent.DetailError -> {
                    data.error.message?.let { errorToast(it) }
                }

                is AppEvent.DetailReady -> {
                    findNavController().navigate(
                        ListFragmentDirections.actionListToDetail(),
                        FragmentNavigatorExtras(constraintLayout to constraintLayout.transitionName)
                    )
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        adapter.addLoadStateListener(loadStateListener)
    }

    override fun onPause() {
        super.onPause()
        adapter.removeLoadStateListener(loadStateListener)
    }
}