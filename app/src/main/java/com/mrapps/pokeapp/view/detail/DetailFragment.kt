package com.mrapps.pokeapp.view.detail

import android.os.Bundle
import android.text.Html
import android.transition.TransitionInflater
import android.view.View
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.mrapps.pokeapp.R
import com.mrapps.pokeapp.state.AppState
import com.mrapps.pokeapp.viewModel.AppViewModel
import io.uniflow.androidx.flow.onStates
import kotlinx.android.synthetic.main.fragment_detail.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class DetailFragment : Fragment(R.layout.fragment_detail) {

    private val appViewModel: AppViewModel by sharedViewModel()
    private var adapter = TypesAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(R.transition.shared_transition)
    }

    private fun onSuccessData() {
        header.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
        errorView.visibility = View.GONE
    }

    private fun onError() {
        header.visibility = View.GONE
        progressBar.visibility = View.GONE
        errorView.visibility = View.VISIBLE
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvCategory.adapter = adapter
        toolbar.setNavigationOnClickListener { activity?.onBackPressed() }

        onStates(appViewModel) { state ->
            when (state) {
                is AppState.Pokemon -> {

                    val pokemon = state.pokemon
                    val detail = state.detail

                    onSuccessData()
                    toolbar.title = "Pokemon Details"

                    Glide.with(this@DetailFragment.requireContext())
                        .load(pokemon.getImageUrl())
                        .into(pokemonImage)

                    pokemonName.text = detail.name
                    pokemonHeight.text = detail.getFormattedHeight()
                    pokemonWeight.text = detail.getFormattedWeight()
                    adapter.setData(detail.types)
                    adapter.notifyDataSetChanged()

                    stats.text =
                        HtmlCompat.fromHtml(detail.stats.joinToString("<br/>") { stat -> "&#8226 ${stat.stat.name}: ${stat.baseStat}" }, HtmlCompat.FROM_HTML_MODE_LEGACY)

                }
                is AppState.PokemonNotLoaded -> {
                    with(state.pokemon) {
                    toolbar.title = name
                    onError()
                    }
                }
            }

        }
    }


}