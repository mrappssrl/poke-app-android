package com.mrapps.pokeapp.view.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mrapps.pokeapp.R
import com.mrapps.pokeapp.model.Type
import com.mrapps.pokeapp.model.TypeValue


class TypesAdapter : RecyclerView.Adapter<TypesAdapter.TypeViewHolder>() {

    private var list: List<TypeValue> = emptyList()

    fun setData(types: List<TypeValue>) {
        list = types
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TypeViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TypeViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: TypeViewHolder, position: Int) {
        val type: Type = list[position].type
        holder.bind(type)
    }

    override fun getItemCount(): Int = list.size

    inner class TypeViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.types_item, parent, false)) {
        private var pokemonType: TextView? = null

        init {
            pokemonType = itemView.findViewById(R.id.pokemonType)
        }

        fun bind(type: Type) {
            pokemonType?.text = type.name
        }

    }

}