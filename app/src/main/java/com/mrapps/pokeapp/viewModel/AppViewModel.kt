package com.mrapps.pokeapp.viewModel

import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.mrapps.pokeapp.event.AppEvent
import com.mrapps.pokeapp.model.Pokemon
import com.mrapps.pokeapp.repository.Repository
import com.mrapps.pokeapp.state.AppState
import io.uniflow.androidx.flow.AndroidDataFlow
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class AppViewModel(
    private val repo: Repository
) : AndroidDataFlow() {

    private var searchJob: Job? = null
    fun search() {
        searchJob?.cancel()
        searchJob = viewModelScope.launch {
            repo.getPokemonsStream()
                .cachedIn(viewModelScope).collectLatest {
                sendPagingData(it)
            }
        }
    }

    init {
        search()
    }

    private fun sendPagingData(pagingData: PagingData<Pokemon>) = action {
        sendEvent(AppEvent.SendPagingData(pagingData))
    }

    fun openPokemonDetail(pokemon: Pokemon) = action {
        try {
            val detail = repo.getPokemonDetail(pokemon.name)
            setState { AppState.Pokemon(pokemon, detail) }
            sendEvent(AppEvent.DetailReady)
        } catch (t: Throwable) {
            setState { AppState.PokemonNotLoaded(pokemon) }
            sendEvent(AppEvent.DetailError(t))
        }
    }


}

