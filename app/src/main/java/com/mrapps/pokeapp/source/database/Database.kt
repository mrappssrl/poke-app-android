package com.mrapps.pokeapp.source.database

import android.content.Context
import androidx.room.*
import com.mrapps.pokeapp.model.Pokemon
import com.mrapps.pokeapp.model.PokemonDetail
import com.mrapps.pokeapp.model.RemoteKeys

@Database(
    entities = [
        Pokemon::class,
        PokemonDetail::class,
        RemoteKeys::class
    ],
    version = 1,
    exportSchema = true
)
@TypeConverters(Converter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getDao(): Dao

    companion object {
        fun buildAppDatabase(context: Context): AppDatabase {
            return Room
                .databaseBuilder<AppDatabase>(context, AppDatabase::class.java, "pokapp-db")
                .fallbackToDestructiveMigration()
                .build()
        }

        fun buildDao(database: AppDatabase): Dao {
            return database.getDao()
        }
    }
}