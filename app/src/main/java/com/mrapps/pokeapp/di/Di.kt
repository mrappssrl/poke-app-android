package com.mrapps.pokeapp.di

import com.mrapps.pokeapp.repository.Repository
import com.mrapps.pokeapp.source.database.AppDatabase
import com.mrapps.pokeapp.source.network.Network
import com.mrapps.pokeapp.viewModel.AppViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object Di {

    private val sourcesModule = module {
        single { Network.buildRetrofit() }
        factory { Network.buildApi(get()) }
        single { AppDatabase.buildAppDatabase(androidContext()) }
        factory { AppDatabase.buildDao(get()) }
    }

    private val repositoryModule = module {
        factory { Repository(get(), get()) }
    }

    private val viewModelModule = module {
        viewModel { AppViewModel(get()) }
    }


    val modules
        get() = listOf(
            sourcesModule,
            repositoryModule,
            viewModelModule
        )
}